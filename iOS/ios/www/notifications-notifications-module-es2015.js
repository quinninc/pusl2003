(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["notifications-notifications-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/notifications/notifications.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/notifications/notifications.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>\r\n      Notices\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n<ion-refresher slot=\"fixed\" (ionRefresh)=\"getNotices($event)\">\r\n  <ion-refresher-content></ion-refresher-content>\r\n</ion-refresher>\r\n<ion-card color=\"light\" text-center *ngFor=\"let Notice of Notices\">\r\n  <ion-card-header>\r\n    <ion-card-title>\r\n      {{ Notice.noticeTitle }}\r\n    </ion-card-title>\r\n    <ion-card-subtitle>{{ Notice.createdDateTime }}</ion-card-subtitle>\r\n  </ion-card-header>\r\n  <ion-card-content>\r\n    <img [src]=\"Notice.coverImageFilePath\" align=\"middle\"/>\r\n    <p>{{ Notice.noticeDescription }}</p>\r\n  </ion-card-content>\r\n</ion-card>\r\n</ion-content>\r\n\r\n");

/***/ }),

/***/ "./src/app/notifications/notifications.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/notifications/notifications.module.ts ***!
  \*******************************************************/
/*! exports provided: notificationsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "notificationsPageModule", function() { return notificationsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _notifications_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./notifications.page */ "./src/app/notifications/notifications.page.ts");







let notificationsPageModule = class notificationsPageModule {
};
notificationsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _notifications_page__WEBPACK_IMPORTED_MODULE_6__["notificationsPage"] }])
        ],
        declarations: [_notifications_page__WEBPACK_IMPORTED_MODULE_6__["notificationsPage"]]
    })
], notificationsPageModule);



/***/ }),

/***/ "./src/app/notifications/notifications.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/notifications/notifications.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25vdGlmaWNhdGlvbnMvbm90aWZpY2F0aW9ucy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/notifications/notifications.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/notifications/notifications.page.ts ***!
  \*****************************************************/
/*! exports provided: notificationsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "notificationsPage", function() { return notificationsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../services.service */ "./src/app/services.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/es2015/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/network/ngx */ "./node_modules/@ionic-native/network/ngx/index.js");





let notificationsPage = class notificationsPage {
    constructor(network, database, Service) {
        this.network = network;
        this.database = database;
        this.Service = Service;
        this.autorefresh(event);
    }
    ngOnInit() {
        this.Service.NoticesPull().subscribe(data => {
            this.Notices = data.map(e => {
                return {
                    id: e.payload.doc.id,
                    isEdit: false,
                    noticeTitle: e.payload.doc.data()['noticeTitle'],
                    coverImageFilePath: e.payload.doc.data()['coverImageFilePath'],
                    noticeDescription: e.payload.doc.data()['noticeDescription'],
                    createdDateTime: e.payload.doc.data()['createdDateTime'],
                };
            });
            console.log(this.Notices);
        });
    }
    getNotices(event) {
        console.log('Begin async operation');
        setTimeout(() => {
            this.Service.NoticesPull().subscribe(data => {
                this.Notices = data.map(e => {
                    return {
                        id: e.payload.doc.id,
                        isEdit: false,
                        noticeTitle: e.payload.doc.data()['noticeTitle'],
                        coverImageFilePath: e.payload.doc.data()['coverImageFilePath'],
                        noticeDescription: e.payload.doc.data()['noticeDescription'],
                        createdDateTime: e.payload.doc.data()['createdDateTime'],
                    };
                });
            });
            console.log('Async operation has ended');
            event.target.complete();
        }, 3000);
    }
    autorefresh(event) {
        this.Service.NoticesPull().subscribe(data => {
            this.Notices = data.map(e => {
                return {
                    id: e.payload.doc.id,
                    isEdit: false,
                    noticeTitle: e.payload.doc.data()['noticeTitle'],
                    coverImageFilePath: e.payload.doc.data()['coverImageFilePath'],
                    noticeDescription: e.payload.doc.data()['noticeDescription'],
                    createdDateTime: e.payload.doc.data()['createdDateTime'],
                };
            });
        });
    }
};
notificationsPage.ctorParameters = () => [
    { type: _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_4__["Network"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] },
    { type: _services_service__WEBPACK_IMPORTED_MODULE_1__["ServicesService"] }
];
notificationsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-notifications',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./notifications.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/notifications/notifications.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./notifications.page.scss */ "./src/app/notifications/notifications.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_4__["Network"],
        _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
        _services_service__WEBPACK_IMPORTED_MODULE_1__["ServicesService"]])
], notificationsPage);



/***/ })

}]);
//# sourceMappingURL=notifications-notifications-module-es2015.js.map