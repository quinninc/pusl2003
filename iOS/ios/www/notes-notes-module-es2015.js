(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["notes-notes-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/notes/notes.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/notes/notes.page.html ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar color=\"warning\">\r\n    <ion-title>\r\n      Notes\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content color=\"dark\">\r\n\r\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"getNotes($event)\">\r\n    <ion-refresher-content></ion-refresher-content>\r\n  </ion-refresher>\r\n\r\n\r\n  <div class=\"bg-img\">\r\n    <h1><b>Take you notes here!</b></h1>\r\n    <h5>{{ currentDate }}</h5>\r\n\r\n    <ion-card *ngFor=\"let Note of Notes\">\r\n      <ion-item-sliding>\r\n        <ion-item lines=\"none\">\r\n          <ion-checkbox (ionChange)=\"changeCheckState(Note)\" color=\"success\" [(ngModel)]=\"Note.checked\" slot=\"start\"></ion-checkbox>\r\n          <ion-label>\r\n            <h2 *ngIf=\"!Note.checked\">{{ Note.text }}</h2>\r\n            <h2 *ngIf=\"Note.checked\" style=\"text-decoration:line-through;\">{{ Note.text }}</h2>\r\n            <p>{{ Note.hour }}</p>\r\n          </ion-label>\r\n        </ion-item>\r\n        <ion-item-options side=\"end\">\r\n          <ion-item-option color=\"danger\" (click)=\"deleteNote(Note)\">\r\n            <ion-icon name=\"trash\" slot=\"icon-only\"></ion-icon>\r\n          </ion-item-option>\r\n        </ion-item-options>\r\n      </ion-item-sliding>\r\n    </ion-card>\r\n\r\n    <ion-card *ngIf=\"addNote\">\r\n      <ion-item lines=\"none\">\r\n        <ion-input [(ngModel)]=\"Note\" placeholder=\"Take your note\"></ion-input>\r\n        <ion-button (click)=\"addNoteToFirebase()\" shape=\"round\" color=\"light\" slot=\"end\">\r\n          <ion-icon slot=\"icon-only\" name=\"add\"></ion-icon>\r\n        </ion-button>\r\n      </ion-item>\r\n    </ion-card>\r\n\r\n    <ion-button *ngIf=\"!addNote\" (click)=\"showForm()\" expand=\"block\" class=\"add-button\" color=\"light\">\r\n      <ion-icon name=\"add\" slot=\"start\"></ion-icon>\r\n      Add a Note\r\n    </ion-button>\r\n\r\n    <ion-button *ngIf=\"addNote\" (click)=\"showForm()\" expand=\"block\" class=\"add-button\" color=\"danger\">\r\n      <ion-icon name=\"close\" slot=\"start\"></ion-icon>\r\n      Discard Note\r\n    </ion-button>\r\n  </div>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/notes/notes.module.ts":
/*!***************************************!*\
  !*** ./src/app/notes/notes.module.ts ***!
  \***************************************/
/*! exports provided: notesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "notesPageModule", function() { return notesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _notes_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./notes.page */ "./src/app/notes/notes.page.ts");







let notesPageModule = class notesPageModule {
};
notesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _notes_page__WEBPACK_IMPORTED_MODULE_6__["notesPage"] }])
        ],
        declarations: [_notes_page__WEBPACK_IMPORTED_MODULE_6__["notesPage"]]
    })
], notesPageModule);



/***/ }),

/***/ "./src/app/notes/notes.page.scss":
/*!***************************************!*\
  !*** ./src/app/notes/notes.page.scss ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".bg-img {\n  padding: 1px;\n  height: 100%;\n  width: 100%;\n  background-image: url('notes.jpg');\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\nion-card {\n  margin-bottom: 0px;\n  margin-top: 6px;\n}\n\nh1 {\n  margin-left: 20px;\n  font-size: 30px;\n}\n\nh5 {\n  margin-left: 20px;\n  margin-top: -7px;\n  font-size: medium;\n  margin-bottom: 15px;\n  text-transform: capitalize;\n}\n\n.add-button {\n  position: absolute;\n  bottom: 5px;\n  width: 96%;\n  margin-left: 2%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbm90ZXMvQzpcXFVzZXJzXFxyYW51bFxcRG9jdW1lbnRzXFxHaXRIdWJcXFVPUF9TRV9ZMlMxLVBVU0wyMDAzX0lOVEVHUkFUSU5HX1BST0pFQ1RcXEFwcGxpY2F0aW9uXFxNb2JpbGUgQXBwc1xcV29ya3NwYWNlXFxOU0JNIEh1Yi9zcmNcXGFwcFxcbm90ZXNcXG5vdGVzLnBhZ2Uuc2NzcyIsInNyYy9hcHAvbm90ZXMvbm90ZXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0NBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0FDQ0o7O0FEQ0E7RUFDSSxrQkFBQTtFQUNBLGVBQUE7QUNFSjs7QURBQTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtBQ0dKOztBRERBO0VBQ0ksaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSwwQkFBQTtBQ0lKOztBREZBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7QUNLSiIsImZpbGUiOiJzcmMvYXBwL25vdGVzL25vdGVzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5iZy1pbWcge1xyXG4gICAgcGFkZGluZzogMXB4O1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy4uLy4uL2Fzc2V0cy9ub3Rlcy5qcGcnKTtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG59XHJcbmlvbi1jYXJkIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgIG1hcmdpbi10b3A6IDZweDtcclxufVxyXG5oMSB7XHJcbiAgICBtYXJnaW4tbGVmdDogMjBweDtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxufVxyXG5oNSB7XHJcbiAgICBtYXJnaW4tbGVmdDogMjBweDtcclxuICAgIG1hcmdpbi10b3A6IC03cHg7XHJcbiAgICBmb250LXNpemU6IG1lZGl1bTtcclxuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxufVxyXG4uYWRkLWJ1dHRvbiB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBib3R0b206IDVweDtcclxuICAgIHdpZHRoOiA5NiU7XHJcbiAgICBtYXJnaW4tbGVmdDogMiU7XHJcbn0iLCIuYmctaW1nIHtcbiAgcGFkZGluZzogMXB4O1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi9hc3NldHMvbm90ZXMuanBnXCIpO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuXG5pb24tY2FyZCB7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgbWFyZ2luLXRvcDogNnB4O1xufVxuXG5oMSB7XG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xuICBmb250LXNpemU6IDMwcHg7XG59XG5cbmg1IHtcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG4gIG1hcmdpbi10b3A6IC03cHg7XG4gIGZvbnQtc2l6ZTogbWVkaXVtO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cblxuLmFkZC1idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogNXB4O1xuICB3aWR0aDogOTYlO1xuICBtYXJnaW4tbGVmdDogMiU7XG59Il19 */");

/***/ }),

/***/ "./src/app/notes/notes.page.ts":
/*!*************************************!*\
  !*** ./src/app/notes/notes.page.ts ***!
  \*************************************/
/*! exports provided: notesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "notesPage", function() { return notesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/es2015/index.js");



let notesPage = class notesPage {
    constructor(database) {
        this.database = database;
        this.Note = '';
        this.Notes = [];
        const date = new Date();
        const options = { weekday: 'long', month: 'long', day: 'numeric' };
        this.currentDate = date.toLocaleDateString('en-GB', options);
        this.autorefresh(event);
    }
    showForm() {
        this.addNote = !this.addNote;
        this.Note = '';
    }
    addNoteToFirebase() {
        this.database.list('/Notes/').push({
            text: this.Note,
            date: new Date().toISOString(),
            checked: false
        });
        this.showForm();
    }
    changeCheckState(ev) {
        console.log('checked: ' + ev.checked);
        this.database.object('/Notes/' + ev.key + '/checked/').set(ev.checked);
    }
    deleteNote(Note) {
        this.database.list('/Notes/').remove(Note.key);
    }
    getNotes(event) {
        console.log('Begin async operation');
        setTimeout(() => {
            this.database.list('/Notes/').snapshotChanges(['child_added', 'child_removed']).subscribe(actions => {
                this.Notes = [];
                actions.forEach(action => {
                    this.Notes.push({
                        key: action.key,
                        text: action.payload.exportVal().text,
                        hour: action.payload.exportVal().date.substring(11, 16),
                        checked: action.payload.exportVal().checked
                    });
                });
            });
            console.log('Async operation has ended');
            event.target.complete();
        }, 3000);
    }
    autorefresh(event) {
        this.database.list('/Notes/').snapshotChanges(['child_added', 'child_removed']).subscribe(actions => {
            this.Notes = [];
            actions.forEach(action => {
                this.Notes.push({
                    key: action.key,
                    text: action.payload.exportVal().text,
                    hour: action.payload.exportVal().date.substring(11, 16),
                    checked: action.payload.exportVal().checked
                });
            });
        });
    }
};
notesPage.ctorParameters = () => [
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"] }
];
notesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-notes',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./notes.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/notes/notes.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./notes.page.scss */ "./src/app/notes/notes.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"]])
], notesPage);



/***/ })

}]);
//# sourceMappingURL=notes-notes-module-es2015.js.map