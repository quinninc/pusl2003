(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- SOURCE CODE WRITTEN BY R.P. LADDUWAHETTY, President of team QUINN -->\r\n<ion-header  slot=\"fixed\">\r\n   <ion-toolbar>\r\n    <ion-title >\r\n  <!-- CODE BLOCK FETCHES UNIVERISTY LOGO AND ALIGNS IT TO CENTER USING CSS,\r\n    NOTE:SOME HTML ALIGNMENT SYNTAXES AREN'T SUPPORTED IN HTML5 -->\r\n      <img src=\"/assets/nsbm.png\" alt=\"NSBM GREEN UNIVERSITY\" height=\"70px\" style=\"display: block;margin-left: auto;margin-right: auto;\"/>\r\n    </ion-title>\r\n  </ion-toolbar>\r\n      <p style=\"color: dodgerblue;margin:0;text-align: center;font-size: small;font-weight: 300;font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;\">{{userEmail}}</p>\r\n    </ion-header>\r\n\r\n<ion-content style=\"align-items: center;background-color: white;\">\r\n\r\n     <!-- WEATHER BAR -->\r\n     <div>\r\n     <object type=\"text/html\" data=\"/assets/weather.html\" style=\"width:100%;padding: 0;\"></object>\r\n     </div>\r\n     <!-- WEATHER BAR -->\r\n\r\n     <!--LMS(MOODLE) -->\r\n     <ion-card style=\"height:30%;text-align:center;\" (click)=\"openLMS()\">\r\n     <img style=\"height:70%;\"alt=\"svgImg\" src=\"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHg9IjBweCIgeT0iMHB4Igp3aWR0aD0iNTAwIiBoZWlnaHQ9IjUwMCIKdmlld0JveD0iMCAwIDQ4IDQ4IgpzdHlsZT0iIGZpbGw6IzAwMDAwMDsiPjxwYXRoIGZpbGw9IiNmZmFiNDAiIGQ9Ik0zMy41LDE2Yy0yLjUsMC00LjgsMS02LjUsMi42QzI1LjMsMTcsMjMsMTYsMjAuNSwxNmMtNS4yLDAtOS41LDQuMy05LjUsOS41VjM3aDZWMjQuNSBjMC0xLjksMS42LTMuNSwzLjUtMy41czMuNSwxLjYsMy41LDMuNVYzN2g2VjI0LjVjMC0xLjksMS42LTMuNSwzLjUtMy41czMuNSwxLjYsMy41LDMuNVYzN2g2VjI1LjVDNDMsMjAuMywzOC43LDE2LDMzLjUsMTZ6Ij48L3BhdGg+PHBhdGggZD0iTTUuNSAxNi4ySDYuNVYzMkg1LjV6Ij48L3BhdGg+PHBhdGggZmlsbD0iIzQyNDI0MiIgZD0iTTIyLDEzYzEuMSwwLjQsMi42LDIsMywzYy0xLjgsMS43LTIuNiwyLjktMyw2Yy0wLjEsMS4xLTAuOSwxLjctMiwxYy0zLjEtMS45LTYtMi04LTIgYy0xLTEtMC41LTMuNywwLTVsNiwxTDIyLDEzeiI+PC9wYXRoPjxwYXRoIGZpbGw9IiM2MTYxNjEiIGQ9Ik0xOCwxN0g0bDExLTdoMTRMMTgsMTd6Ij48L3BhdGg+PHBhdGggZmlsbD0iIzQyNDI0MiIgZD0iTTcuNSwzMGMwLTIuMi0wLjctNC0xLjUtNHMtMS41LDEuOC0xLjUsNHMwLjcsNCwxLjUsNFM3LjUsMzIuMiw3LjUsMzB6Ij48L3BhdGg+PC9zdmc+\">\r\n        <ion-card-title style=\"text-align:center;\">LMS Moodle</ion-card-title>\r\n    </ion-card>\r\n     <!--LMS(MOODLE) -->\r\n\r\n    <!--EMAIL -->\r\n     <ion-card style=\"height:30%;text-align:center;\"  (click)=\"openOutlook()\">\r\n     <img style=\"height:70%;\"alt=\"svgImg\" src=\"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHg9IjBweCIgeT0iMHB4Igp3aWR0aD0iNDgwIiBoZWlnaHQ9IjQ4MCIKdmlld0JveD0iMCAwIDQ4IDQ4IgpzdHlsZT0iIGZpbGw6IzAwMDAwMDsiPjxwYXRoIGZpbGw9IiMxYTIzN2UiIGQ9Ik00My42MDcsMjMuNzUybC03LjE2Mi00LjE3MnYxMS41OTRINDR2LTYuNzM4QzQ0LDI0LjE1NSw0My44NSwyMy44OTQsNDMuNjA3LDIzLjc1MnoiPjwvcGF0aD48cGF0aCBmaWxsPSIjMGM0OTk5IiBkPSJNMzMuOTE5LDguODRoOS4wNDZWNy43MzJDNDIuOTY1LDYuNzc1LDQyLjE5LDYsNDEuMjM0LDZIMTcuNjY3Yy0wLjk1NiwwLTEuNzMyLDAuNzc1LTEuNzMyLDEuNzMyIFY4Ljg0aDkuMDA1SDMzLjkxOXoiPjwvcGF0aD48cGF0aCBmaWxsPSIjMGY3M2Q5IiBkPSJNMzMuOTE5LDMzLjUyMmg3LjMxNGMwLjk1NiwwLDEuNzMyLTAuNzc1LDEuNzMyLTEuNzMydi02LjgyN2gtOS4wNDZWMzMuNTIyeiI+PC9wYXRoPjxwYXRoIGZpbGw9IiMwZjQzOWQiIGQ9Ik0xNS45MzYsMjQuOTY0djYuODI3YzAsMC45NTYsMC43NzUsMS43MzIsMS43MzIsMS43MzJoNy4yNzN2LTguNTU4SDE1LjkzNnoiPjwvcGF0aD48cGF0aCBmaWxsPSIjMmVjZGZkIiBkPSJNMzMuOTE5IDguODRINDIuOTY0OTk5OTk5OTk5OTk2VjE2Ljg2Njk5OTk5OTk5OTk5N0gzMy45MTl6Ij48L3BhdGg+PHBhdGggZmlsbD0iIzFjNWZiMCIgZD0iTTE1LjkzNiA4Ljg0SDI0Ljk0MTAwMDAwMDAwMDAwM1YxNi44NjY5OTk5OTk5OTk5OTdIMTUuOTM2eiI+PC9wYXRoPjxwYXRoIGZpbGw9IiMxNDY3YzciIGQ9Ik0yNC45NCAyNC45NjRIMzMuOTE5VjMzLjUyMkgyNC45NHoiPjwvcGF0aD48cGF0aCBmaWxsPSIjMTY5MGQ1IiBkPSJNMjQuOTQgOC44NEgzMy45MTlWMTYuODY2OTk5OTk5OTk5OTk3SDI0Ljk0eiI+PC9wYXRoPjxwYXRoIGZpbGw9IiMxYmI0ZmYiIGQ9Ik0zMy45MTkgMTYuODY3SDQyLjk2NDk5OTk5OTk5OTk5NlYyNC45NjNIMzMuOTE5eiI+PC9wYXRoPjxwYXRoIGZpbGw9IiMwNzRkYWYiIGQ9Ik0xNS45MzYgMTYuODY3SDI0Ljk0MTAwMDAwMDAwMDAwM1YyNC45NjNIMTUuOTM2eiI+PC9wYXRoPjxwYXRoIGZpbGw9IiMyMDc2ZDQiIGQ9Ik0yNC45NCAxNi44NjdIMzMuOTE5VjI0Ljk2M0gyNC45NHoiPjwvcGF0aD48cGF0aCBmaWxsPSIjMmVkMGZmIiBkPSJNMTUuNDQxLDQyYzAuNDYzLDAsMjYuODcsMCwyNi44NywwQzQzLjI0NCw0Miw0NCw0MS4yNDQsNDQsNDAuMzExVjI0LjQzOCBjMCwwLTAuMDMsMC42NTgtMS43NTEsMS42MTdjLTEuMywwLjcyNC0yNy41MDUsMTUuNTExLTI3LjUwNSwxNS41MTFTMTQuOTc4LDQyLDE1LjQ0MSw0MnoiPjwvcGF0aD48cGF0aCBmaWxsPSIjMTM5ZmUyIiBkPSJNNDIuMjc5LDQxLjk5N2MtMC4xNjEsMC0yNi41OSwwLjAwMy0yNi41OSwwLjAwM0MxNC43NTYsNDIsMTQsNDEuMjQ0LDE0LDQwLjMxMVYyNS4wNjcgbDI5LjM2MywxNi41NjJDNDMuMTE4LDQxLjgyNSw0Mi44MDcsNDEuOTk3LDQyLjI3OSw0MS45OTd6Ij48L3BhdGg+PHBhdGggZmlsbD0iIzAwNDg4ZCIgZD0iTTIyLjMxOSwzNEg1LjY4MUM0Ljc1MywzNCw0LDMzLjI0Nyw0LDMyLjMxOVYxNS42ODFDNCwxNC43NTMsNC43NTMsMTQsNS42ODEsMTRoMTYuNjM4IEMyMy4yNDcsMTQsMjQsMTQuNzUzLDI0LDE1LjY4MXYxNi42MzhDMjQsMzMuMjQ3LDIzLjI0NywzNCwyMi4zMTksMzR6Ij48L3BhdGg+PHBhdGggZmlsbD0iI2ZmZiIgZD0iTTEzLjkxNCwxOC43MzRjLTMuMTMxLDAtNS4wMTcsMi4zOTItNS4wMTcsNS4zNDNjMCwyLjk1MSwxLjg3OSw1LjM0Miw1LjAxNyw1LjM0MiBjMy4xMzksMCw1LjAxNy0yLjM5Miw1LjAxNy01LjM0MkMxOC45MzEsMjEuMTI2LDE3LjA0NSwxOC43MzQsMTMuOTE0LDE4LjczNHogTTEzLjkxNCwyNy42MTZjLTEuNzc2LDAtMi44MzgtMS41ODQtMi44MzgtMy41MzkgczEuMDY3LTMuNTM5LDIuODM4LTMuNTM5YzEuNzcxLDAsMi44MzksMS41ODUsMi44MzksMy41MzlTMTUuNjg5LDI3LjYxNiwxMy45MTQsMjcuNjE2eiI+PC9wYXRoPjwvc3ZnPg==\">\r\n        <ion-card-title style=\"text-align:center;\">Email</ion-card-title>\r\n    </ion-card>\r\n    <!--EMAIL -->\r\n\r\n     <!--OFFICE 365 -->\r\n     <ion-card style=\"height:30%;text-align:center;\" (click)=\"openOffice365()\">\r\n     <img style=\"height:70%;\" alt=\"svgImg\" src=\"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHg9IjBweCIgeT0iMHB4Igp3aWR0aD0iNDgwIiBoZWlnaHQ9IjQ4MCIKdmlld0JveD0iMCAwIDQ4IDQ4IgpzdHlsZT0iIGZpbGw6IzAwMDAwMDsiPjxwYXRoIGZpbGw9IiNlNjRhMTkiIGQ9Ik03IDEyTDI5IDQgNDEgNyA0MSA0MSAyOSA0NCA3IDM2IDI5IDM5IDI5IDEwIDE1IDEzIDE1IDMzIDcgMzZ6Ij48L3BhdGg+PC9zdmc+\">\r\n        <ion-card-title style=\"text-align:center;\">Office 365</ion-card-title>\r\n    </ion-card>\r\n     <!--OFFICE 365 -->\r\n\r\n      <!--SCHEDULE -->\r\n     <ion-card style=\"height:30%;text-align:center;\">\r\n     <img style=\"height:70%;\" alt=\"svgImg\" src=\"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHg9IjBweCIgeT0iMHB4Igp3aWR0aD0iNDgwIiBoZWlnaHQ9IjQ4MCIKdmlld0JveD0iMCAwIDQ4IDQ4IgpzdHlsZT0iIGZpbGw6IzAwMDAwMDsiPjxwYXRoIGZpbGw9IiNDRkQ4REMiIGQ9Ik01LDM4VjE0aDM4djI0YzAsMi4yLTEuOCw0LTQsNEg5QzYuOCw0Miw1LDQwLjIsNSwzOHoiPjwvcGF0aD48cGF0aCBmaWxsPSIjRjQ0MzM2IiBkPSJNNDMsMTB2Nkg1di02YzAtMi4yLDEuOC00LDQtNGgzMEM0MS4yLDYsNDMsNy44LDQzLDEweiI+PC9wYXRoPjxwYXRoIGZpbGw9IiNCNzFDMUMiIGQ9Ik0zMyA3QTMgMyAwIDEgMCAzMyAxMyAzIDMgMCAxIDAgMzMgN3pNMTUgN0EzIDMgMCAxIDAgMTUgMTMgMyAzIDAgMSAwIDE1IDd6Ij48L3BhdGg+PHBhdGggZmlsbD0iI0IwQkVDNSIgZD0iTTMzIDNjLTEuMSAwLTIgLjktMiAydjVjMCAxLjEuOSAyIDIgMnMyLS45IDItMlY1QzM1IDMuOSAzNC4xIDMgMzMgM3pNMTUgM2MtMS4xIDAtMiAuOS0yIDJ2NWMwIDEuMS45IDIgMiAyczItLjkgMi0yVjVDMTcgMy45IDE2LjEgMyAxNSAzeiI+PC9wYXRoPjxnPjxwYXRoIGZpbGw9IiM5MEE0QUUiIGQ9Ik0xMyAyMEgxN1YyNEgxM3pNMTkgMjBIMjNWMjRIMTl6TTI1IDIwSDI5VjI0SDI1ek0zMSAyMEgzNVYyNEgzMXpNMTMgMjZIMTdWMzBIMTN6TTE5IDI2SDIzVjMwSDE5ek0yNSAyNkgyOVYzMEgyNXpNMzEgMjZIMzVWMzBIMzF6TTEzIDMySDE3VjM2SDEzek0xOSAzMkgyM1YzNkgxOXpNMjUgMzJIMjlWMzZIMjV6TTMxIDMySDM1VjM2SDMxeiI+PC9wYXRoPjwvZz48L3N2Zz4=\">\r\n     <ion-card-title style=\"text-align:center;\">Schedule</ion-card-title>\r\n    </ion-card>\r\n     <!--SCHEDULE -->\r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: homePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "homePageModule", function() { return homePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");







let homePageModule = class homePageModule {
};
homePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _home_page__WEBPACK_IMPORTED_MODULE_6__["homePage"] }])
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["homePage"]]
    })
], homePageModule);



/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".welcome-card img {\n  max-height: 35vh;\n  overflow: hidden;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9DOlxcVXNlcnNcXHJhbnVsXFxEb2N1bWVudHNcXEdpdEh1YlxcVU9QX1NFX1kyUzEtUFVTTDIwMDNfSU5URUdSQVRJTkdfUFJPSkVDVFxcQXBwbGljYXRpb25cXE1vYmlsZSBBcHBzXFxXb3Jrc3BhY2VcXE5TQk0gSHViL3NyY1xcYXBwXFxob21lXFxob21lLnBhZ2Uuc2NzcyIsInNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdCQUFBO0VBQ0EsZ0JBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud2VsY29tZS1jYXJkIGltZyB7XHJcbiAgbWF4LWhlaWdodDogMzV2aDtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcbiIsIi53ZWxjb21lLWNhcmQgaW1nIHtcbiAgbWF4LWhlaWdodDogMzV2aDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: homePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "homePage", function() { return homePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/ngx/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _services_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services.service */ "./src/app/services.service.ts");
/* harmony import */ var _theme_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../theme.service */ "./src/app/theme.service.ts");
/* harmony import */ var _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/network/ngx */ "./node_modules/@ionic-native/network/ngx/index.js");







let homePage = class homePage {
    constructor(navCtrl, authService, ThemeService, InAppBrowser, network) {
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.ThemeService = ThemeService;
        this.InAppBrowser = InAppBrowser;
        this.network = network;
    }
    ngOnInit() {
        if (this.authService.userDetails()) {
            this.userEmail = this.authService.userDetails().email;
        }
        else {
            this.navCtrl.navigateBack("");
        }
    }
    DarkMode() {
        this.ThemeService.toggleAppTheme();
    }
    openLMS() {
        const options = {
            zoom: 'no',
            location: 'no',
        };
        const browser = this.InAppBrowser.create('https://lmsnew.nsbm.lk/my/', '_blank', options);
    }
    openOutlook() {
        const options = {
            zoom: 'no',
            location: 'no',
        };
        const browser = this.InAppBrowser.create('https://outlook.office.com/mail/inbox', '_blank', options);
    }
    openOffice365() {
        const options = {
            zoom: 'no',
            location: 'no',
        };
        const browser = this.InAppBrowser.create('https://www.office.com/?auth=2', '_blank', options);
    }
    logout() {
        this.authService
            .logoutUser()
            .then(res => {
            console.log(res);
            this.navCtrl.navigateBack("");
        })
            .catch(error => {
            console.log(error);
        });
    }
};
homePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _services_service__WEBPACK_IMPORTED_MODULE_4__["ServicesService"] },
    { type: _theme_service__WEBPACK_IMPORTED_MODULE_5__["ThemeService"] },
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_1__["InAppBrowser"] },
    { type: _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_6__["Network"] }
];
homePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: "app-home",
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
        _services_service__WEBPACK_IMPORTED_MODULE_4__["ServicesService"],
        _theme_service__WEBPACK_IMPORTED_MODULE_5__["ThemeService"],
        _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_1__["InAppBrowser"],
        _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_6__["Network"]])
], homePage);



/***/ })

}]);
//# sourceMappingURL=home-home-module-es2015.js.map